<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Usuarios;
use Faker\Generator;

class UserTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Generator $faker){
        Usuarios::create([
            'nome'        => "André Jálisson",
            'usuario'         => "andrejalisson",
            'email'             => 'andrejalisson@gmail.com',
            'senha'          => password_hash("Aa@31036700.", PASSWORD_DEFAULT),
        ]);

        $i = 1;
        while ($i <= 1000) {
            Usuarios::create([
                'nome'      => $faker->firstName,
                'usuario'   => $faker->userName,
                'email'     => $faker->email,
                'senha'     => password_hash(31036700, PASSWORD_DEFAULT),
            ]);
            echo $i++;
            echo " - ";
        }


    }
}
