<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFornecedoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fornecedores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fantasia', 100);
            $table->string('nome', 100)->nullable();
            $table->string('email', 50)->nullable();
            $table->string('telefone', 11)->nullable();
            $table->string('cnpj', 14)->nullable();
            $table->string('cep', 8)->nullable();
            $table->string('uf', 2)->nullable();
            $table->string('municipio', 50)->nullable();
            $table->string('bairro', 50)->nullable();
            $table->string('logradouro', 50)->nullable();
            $table->string('numero', 50)->nullable();
            $table->integer('tipo')->unsigned()->nullable()->default(3);
            $table->string('complemento', 50)->nullable();
            $table->boolean('status')->nullable()->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fornecedores');
    }
}
