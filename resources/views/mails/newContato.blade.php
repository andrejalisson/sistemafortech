@component('mail::message')
    <h1>Novo contato do site Fortech</h1>
    <p>Nome: {{$contato->nome}}</p>
    <p>E-Mail: {{$contato->email}}</p>
    <p>Celular: {{$contato->celular}}</p>
    <p>Veículo: {{$contato->veiculo}}</p>
    <p>Mensagem: {{$contato->Mensagem}}</p>
@endcomponent
