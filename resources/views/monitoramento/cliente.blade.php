@extends('templates.admin')

@section('css')
@endsection

@section('corpo')
    <script src='https://api.mapbox.com/mapbox-gl-js/v2.3.1/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v2.3.1/mapbox-gl.css' rel='stylesheet' />
    <style>
        #map {
            height:100%;
            width:100%;
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
        }
    </style>
    <div id="map" class="row gy-5 g-xl-8">

    </div>
    <script>
        mapboxgl.accessToken = 'pk.eyJ1IjoiYW5kcmVqYWxpc3NvbiIsImEiOiJja3NvbHJrZTgzc3IyMndvMjNsczkxNDRmIn0.KNZLpSoqt3E3MxYLtR8iyQ';
        const map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [{{$localizacao->longitude}}, {{$localizacao->latitude}}],
            zoom: 18
        });

        const marker1 = new mapboxgl.Marker()
            .setLngLat([{{$localizacao->longitude}}, {{$localizacao->latitude}}])
            .addTo(map);


    </script>

@endsection

@section('js')
@endsection

@section('script')
@endsection
