<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
    <title>{{$title}} | Fortech GPS</title>
    <!-- Stylesheets -->
    <link href="/site/css/bootstrap.css" rel="stylesheet">
    <link href="/site/css/revolution-slider.css" rel="stylesheet">
    <link href="/site/css/style.css" rel="stylesheet">

    <!--Favicon-->
    <link rel="shortcut icon" href="/site/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/site/images/favicon.ico" type="image/x-icon">
    <!-- Responsive -->
    @yield('css')
        <meta name="description" content="Rastreamento Veicular 24h a partir de R$40,00. Recuperação e assistência em casos de roubos ou furtos. Mais proteção para seu carro, moto ou caminhão.">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <meta name="google-site-verification" content="Dr4rHf1h4ej0TY34QsQiyw66t1znvUtIWbsoniGKFyE" />
        <link href="/site/css/responsive.css" rel="stylesheet">
        <link href="/dist/assets/iziToast/dist/css/iziToast.min.css" rel="stylesheet" >
        <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
        <!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
    </head>

    <body>
        <div class="page-wrapper">

            <!-- Preloader -->
            <div class="preloader"></div>

            <!-- Main Header-->
            <header class="main-header">
                <!--Header-Upper-->
                <div class="header-upper">
                    <div class="auto-container">
                        <div class="clearfix">

                            <div class="pull-left logo-outer">
                                <div class="logo"><a href="/"><img src="/site/images/logo-fortechGPS.png" alt="Logo FortecGPS" title="Fortech GPS"></a></div>
                            </div>

                            <div class="pull-right upper-right clearfix">

                                <!--Info Box-->
                                <div class="upper-column info-box">
                                    <div class="icon-box"><span class="flaticon-clock-2"></span></div>
                                    HORÁRIO DE ATENDIMENTO
                                    <div class="light-text">Segunda - Sexta: 8.00h às 18:00h</div>
                                </div>

                                <!--Info Box-->
                                <div class="upper-column info-box">
                                    <div class="icon-box"><span class="flaticon-technology-1"></span></div>
                                    TELEFONE
                                    <div class="light-text"><a href="https://wa.me/558533001816">(85)3300-1816</a></div>
                                </div>

                                <!--Info Box-->
                                <div class="upper-column info-box">
                                    <div class="icon-box"><span class="flaticon-envelope-3"></span></div>
                                    E-MAIL
                                    <div class="light-text">contato@fortechgps.com.br</div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

                <!--Header-Lower-->
                <div class="header-lower">

                    <div class="auto-container">
                        <div class="nav-outer clearfix">
                            <!-- Main Menu -->
                            <nav class="main-menu">
                                <div class="navbar-header">
                                    <!-- Toggle Button -->
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>

                                <div class="navbar-collapse collapse clearfix">
                                    <ul class="navigation clearfix">
                                        @switch($title)
                                            @case("Home")
                                                <li class="current"><a href="/">Home</a></li>
                                                <li><a href="/Planos">Planos</a></li>
                                                <li><a href="/Assistencia">Assistência 24h</a></li>
                                                <li><a href="https://fortechgps.rastrosystem.com.br/">Área do Cliente</a></li>
                                                @break
                                            @case("Planos")
                                                <li><a href="/">Home</a></li>
                                                <li class="current"><a href="/Planos">Planos</a></li>
                                                <li><a href="/Assistencia">Assistência 24h</a></li>
                                                <li><a href="https://fortechgps.rastrosystem.com.br/">Área do Cliente</a></li>
                                                @break
                                            @case("Assistência 24H")
                                                <li><a href="/">Home</a></li>
                                                <li><a href="/Planos">Planos</a></li>
                                                <li class="current"><a href="/Assistencia">Assistência 24h</a></li>
                                                <li><a href="https://fortechgps.rastrosystem.com.br/">Área do Cliente</a></li>
                                                @break
                                            @case("Contato")
                                                <li><a href="/">Home</a></li>
                                                <li><a href="/Planos">Planos</a></li>
                                                <li><a href="/Assistencia">Assistência 24h</a></li>
                                                <li><a href="https://fortechgps.rastrosystem.com.br/">Área do Cliente</a></li>
                                                @break
                                            @default


                                        @endswitch
                                    </ul>
                                </div>
                            </nav>
                            <!-- Main Menu End-->

                            <div class="outer-btn"><a href="/Contato" class="theme-btn consultation">CONTATO</a></div>

                        </div>
                    </div>
                </div>

                <!--Sticky Header-->
                <div class="sticky-header">
                    <div class="auto-container clearfix">
                        <!--Logo-->
                        <div class="logo pull-left">
                            <a href="/" class="img-responsive"><img src="/site/images/logo-fortechGPS-small.png"  alt="Logo Fortech GPS"></a>
                        </div>

                        <!--Right Col-->
                        <div class="right-col pull-right">
                            <!-- Main Menu -->
                            <nav class="main-menu">
                                <div class="navbar-header">
                                    <!-- Toggle Button -->
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>

                                <div class="navbar-collapse collapse clearfix">
                                    <ul class="navigation clearfix">
                                        @switch($title)
                                            @case("Home")
                                                <li class="current"><a href="/">Home</a></li>
                                                <li><a href="/Planos">Planos</a></li>
                                                <li><a href="/Assistencia">Assistência 24h</a></li>
                                                <li><a href="/Contato">Contato</a></li>
                                                <li><a href="https://fortechgps.rastrosystem.com.br/">Área do Cliente</a></li>
                                                @break
                                            @case("Planos")
                                                <li><a href="/">Home</a></li>
                                                <li class="current"><a href="/Planos">Planos</a></li>
                                                <li><a href="/Assistencia">Assistência 24h</a></li>
                                                <li><a href="/Contato">Contato</a></li>
                                                <li><a href="https://fortechgps.rastrosystem.com.br/">Área do Cliente</a></li>
                                                @break
                                            @case("Assistência 24H")
                                                <li><a href="/">Home</a></li>
                                                <li><a href="/Planos">Planos</a></li>
                                                <li class="current"><a href="/Assistencia">Assistência 24h</a></li>
                                                <li><a href="/Contato">Contato</a></li>
                                                <li><a href="https://fortechgps.rastrosystem.com.br/">Área do Cliente</a></li>
                                                @break
                                            @case("Contato")
                                                <li><a href="/">Home</a></li>
                                                <li><a href="/Planos">Planos</a></li>
                                                <li><a href="/Assistencia">Assistência 24h</a></li>
                                                <li class="current"><a href="/Contato">Contato</a></li>
                                                <li><a href="https://fortechgps.rastrosystem.com.br/">Área do Cliente</a></li>
                                                @break
                                            @default

                                        @endswitch

                                    </ul>
                                </div>
                            </nav><!-- Main Menu End-->
                        </div>

                    </div>
                </div>
                <!--End Sticky Header-->

            </header>
            <!--End Main Header -->
            @yield('corpo')
            <!--Main Footer-->
            <footer class="main-footer">
                <!--Footer Bottom-->
                <div class="footer-bottom">
                    <div class="auto-container">
                        <div class="row clearfix">
                            <!--column-->
                            <div class="column col-md-6 col-sm-6 col-xs-12">
                                <div class="copyright">&copy; {{date('Y')}} Fortech GPS. Todos os direitos reservados.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <!--End pagewrapper-->

        <!--Scroll to top-->
        <div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="fa fa-long-arrow-up"></span></div>

        <script src="/site/js/jquery.js"></script>
        <script src="/site/js/bootstrap.min.js"></script>
        <script src="/site/js/revolution.min.js"></script>
        <script src="/site/js/jquery.fancybox.pack.js"></script>
        <script src="/site/js/jquery.fancybox-media.js"></script>
        <script src="/site/js/owl.js"></script>
        <script src="/site/js/wow.js"></script>
        <script src="/site/js/script.js"></script>
        <script src="/site/js/jQuery-Mask/dist/jquery.mask.min.js"></script>
        <script src="/dist/assets/iziToast/dist/js/iziToast.min.js"></script>
        @yield('js')
        @yield('script')
        <script>
           $(document).ready(function(){
                @if (session('sucesso'))
                    iziToast.success({
                        title: ':)',
                        transitionIn: 'bounceInLeft',
                        position: 'topRight',
                        message: "{{session('sucesso')}}",
                    });
                @endif
                @if (session('alerta'))
                    iziToast.warning({
                        title: 'O.o',
                        transitionIn: 'bounceInLeft',
                        position: 'topLeft',
                        message: "{{session('alerta')}}",
                    });
                @endif
                @if (session('erro'))
                    iziToast.error({
                        title: ':X',
                        transitionIn: 'bounceInLeft',
                        position: 'topRight',
                        message: "{{session('erro')}}",
                    });
                @endif

            });
        </script>

    </body>
</html>
