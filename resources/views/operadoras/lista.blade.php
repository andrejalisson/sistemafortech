@extends('templates.admin')

@section('css')
    <link href="/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css"/>

@endsection

@section('corpo')
    <!--begin::Toolbar-->
    <div class="toolbar" id="kt_toolbar">
        <!--begin::Container-->
        <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
            <!--begin::Page title-->
            <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                <!--begin::Title-->
                <h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1">{{$title}}</h1>
                <!--end::Title-->
                <!--begin::Separator-->
                <span class="h-20px border-gray-200 border-start mx-4"></span>
                <!--end::Separator-->
                <!--begin::Breadcrumb-->
                <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-muted">
                        <a href="" class="text-muted text-hover-primary">Cadastro</a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-200 w-5px h-2px"></span>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-muted">Operadoras</li>
                    <!--end::Item-->
                </ul>
                <!--end::Breadcrumb-->
            </div>
            <!--end::Page title-->
            <!--begin::Actions-->
            <div class="d-flex align-items-center py-1">
                <!--begin::Button-->
                <a href="#" class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#addModelo" id="kt_toolbar_primary_button"><i class="fas fa-plus"></i> Adicionar Operadora</a>
                <!--end::Button-->
            </div>
            <!--end::Actions-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Post-->
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container">
            <!--begin::Wrapper-->
            <div class="d-flex flex-stack mb-5">
                <!--begin::Search-->
                <div class="d-flex align-items-center position-relative my-1">
                    <i class="fas fa-search"></i>
                    <input type="text" data-kt-docs-table-filter="search" class="form-control form-control-solid w-250px ps-15" id="pesquisa" placeholder="Pesquisa Operadora"/>
                </div>
                <!--end::Search-->
            </div>
            <!--end::Wrapper-->

            <!--begin::Datatable-->
            <table id="operadora" class="table align-middle table-row-dashed fs-6 gy-5">
                <thead>
                <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                    <th>ID</th>
                    <th>Operadora</th>
                    <th>Multi</th>
                    <th>Status</th>
                    <th class="text-end min-w-100px">Opções</th>
                </tr>
                </thead>
                <tbody class="text-gray-600 fw-bold">
                </tbody>
            </table>
            <!--end::Datatable-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Post-->
    </div>
    <div class="modal fade" id="addModelo" tabindex="-1" aria-hidden="true">
        <!--begin::Modal dialog-->
        <div class="modal-dialog modal-dialog-centered mw-650px">
            <!--begin::Modal content-->
            <div class="modal-content rounded">
                <!--begin::Modal header-->
                <div class="modal-header pb-0 border-0 justify-content-end">
                    <!--begin::Close-->
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <!--begin::Svg Icon | path: icons/duotone/Navigation/Close.svg-->
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                                    <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
                                </g>
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                    </div>
                    <!--end::Close-->
                </div>
                <!--begin::Modal header-->
                <!--begin::Modal body-->
                <div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
                    <!--begin:Form-->
                    <form id="formOperadora" class="form" action="/addOperadora" method="post">
                        <!--begin::Heading-->
                        <div class="mb-13 text-center">
                            <!--begin::Title-->
                            <h1 class="mb-3">Adicionar Operadora</h1>
                            <!--end::Title-->
                        </div>
                        <!--end::Heading-->
                        <!--begin::Input group-->
                        <div class="d-flex flex-column mb-8 fv-row">
                            <!--begin::Label-->
                            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                                <span class="required">Operadora</span>
                                <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Nome da Operadora."></i>
                            </label>
                            <!--end::Label-->
                            <input type="text" class="form-control form-control-solid" placeholder="Entre com o nome da operadora" id="nomeOperadora" name="operadora" />
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="d-flex flex-stack mb-8">
                            <!--begin::Label-->
                            <div class="me-5">
                                <label class="fs-6 fw-bold">Multi Operadora?</label>
                                <div class="fs-7 fw-bold text-muted">Deixe marcado caso seja um chip multi operadora(ex: Algar)</div>
                            </div>
                            <!--end::Label-->
                            <!--begin::Switch-->
                            <label class="form-check form-switch form-check-custom form-check-solid">
                                <span class="form-check-label fw-bold text-muted">Não .</span>
                                <input class="form-check-input" type="checkbox" name="multi" value="1" checked="checked" />
                                <span class="form-check-label fw-bold text-muted">Sim</span>
                            </label>
                            <!--end::Switch-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Actions-->
                        <div class="text-center">
                            <button type="reset" id="kt_modal_new_target_cancel" class="btn btn-light me-3">Cancelar</button>
                            <button type="submit" id="botaoSalvar" class="btn btn-primary">
                                <span class="indicator-label">Salvar</span>
                                <span class="indicator-progress">Aguarde...
                                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                            </button>
                        </div>
                        <!--end::Actions-->
                    </form>
                    <!--end:Form-->
                </div>
                <!--end::Modal body-->
            </div>
            <!--end::Modal content-->
        </div>
        <!--end::Modal dialog-->
    </div>
@endsection

@section('js')
    <script src="/assets/plugins/custom/datatables/datatables.bundle.js"></script>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('#operadora').dataTable({
                "serverSide": true,
                "oLanguage": {
                    "sLengthMenu": "Mostrar _MENU_ registros por página",
                    "sZeroRecords": "Nenhum registro encontrado",
                    "sInfo": "Mostrando _END_ de _TOTAL_ registro(s)",
                    "sInfoEmpty": "Mostrando 0 / 0 de 0 registros",
                    "sInfoFiltered": "(filtrado de _MAX_ registros)",
                    "sSearch": "Pesquisar: ",
                    "oPaginate": {
                        "sFirst": "Início",
                        "sPrevious": "Anterior",
                        "sNext": "Próximo",
                        "sLast": "Último"
                    }
                },
                "ajax":{
                    "url": "{{ url('todasOperadoras') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data":{
                        _token: "{{csrf_token()}}"

                    }
                },
                "columns": [
                    { "data": "id" },
                    { "data": "nome" },
                    { "data": "multi" },
                    { "data": "status" },
                    { "data": "opcoes" }
                ]
            });
            $("#pesquisa").on('keyup', function (){
                $('#operadora').dataTable().fnFilter(this.value);
            });
        });

        function ativar(id) {
            Swal.fire({
                title: 'Ativar',
                text: "Deseja realmente ativar essa operadora?",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText:"Cancelar",
                confirmButtonText: 'Sim, Ativar agora'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "/statusOperadora",
                        type: 'POST',
                        data: {
                            "_token": "{{ csrf_token() }}",
                            id: id
                        },
                        success: function(data) {
                            var obj = jQuery.parseJSON(data);
                            if ($.isEmptyObject(obj.erro)) {
                                $('#operadora').DataTable().ajax.reload();
                                Swal.fire(
                                    'Desativado!',
                                    'A Operadora foi desativada',
                                    'success'
                                )
                            } else {
                                Swal.fire(
                                    'Ops!',
                                    'aconteceu algum erro!!',
                                    'error'
                                )
                            }
                        }
                    });
                }
            })
        }

        function desativar(id) {
            Swal.fire({
                title: 'Desativar',
                text: "Deseja realmente desativar essa operadora?",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText:"Cancelar",
                confirmButtonText: 'Sim, Desativar agora'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "/statusOperadora",
                        type: 'POST',
                        data: {
                            "_token": "{{ csrf_token() }}",
                            id: id
                        },
                        success: function(data) {
                            var obj = jQuery.parseJSON(data);
                            if ($.isEmptyObject(obj.erro)) {
                                $('#operadora').DataTable().ajax.reload();
                                Swal.fire(
                                    'Desativado!',
                                    'A Operadora foi desativada',
                                    'success'
                                )
                            } else {
                                Swal.fire(
                                    'Ops!',
                                    'aconteceu algum erro!!',
                                    'error'
                                )
                            }
                        }
                    });
                }
            })
        }

        $( "#formOperadora" ).submit(function( event ) {
            if($("#nomeOperadora").val() == ""){
                Swal.fire({
                    text: "Desculpe, Para cadastrar a operadora, o requisito mínimo é o nome da operadora!.",
                    icon: "error",
                    buttonsStyling: false,
                    confirmButtonText: "Ok, vou corrigir!",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                });
                event.preventDefault();
            }else{
                var button = document.querySelector("#botaoSalvar");
                var target = document.querySelector("#kt_content");
                var blockUI = new KTBlockUI(target);
                button.addEventListener("click", function() {
                    button.setAttribute("data-kt-indicator", "on");
                    button.disabled = true;
                    blockUI.block();
                    var operadora = $("input[name='operadora']").val();
                    if($("input[name='multi']").checked){
                        var multi = 1;
                    }else{
                        var multi = 0;
                    }
                    $.ajax({
                        url: "/addOperadora",
                        type: 'POST',
                        data: {
                            "_token": "{{ csrf_token() }}",
                            operadora: operadora,
                            multi: multi
                        },
                        success: function(data) {
                            var obj = jQuery.parseJSON(data);
                            console.log(obj.erro);
                            if ($.isEmptyObject(obj.erro)) {
                                button.removeAttribute("data-kt-indicator");
                                button.disabled = false;
                                $('#operadora').DataTable().ajax.reload();
                                Swal.fire({
                                    text: "Operadora Cadastrada com sucesso",
                                    icon: "success",
                                    buttonsStyling: false,
                                    confirmButtonText: "Ok, vamos nessa!!",
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                });
                                blockUI.release();
                            } else {
                                button.removeAttribute('data-kt-indicator');
                                button.disabled = false;
                                blockUI.release();
                                Swal.fire({
                                    text: "Infelizmente algo deu errado, tente novamente",
                                    icon: "error",
                                    buttonsStyling: false,
                                    confirmButtonText: "Ok, entendi!",
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                });
                            }
                        }
                    });
                });
                event.preventDefault();
            }
        });


    </script>
@endsection
