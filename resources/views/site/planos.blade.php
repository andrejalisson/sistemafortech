@extends('templates.site')

@section('css')
@endsection

@section('corpo')

 <!--Price Section-->
 <section class="price-section style-two">
    <div class="auto-container">
        <div class="sec-title">
            <h2>PLANOS QUE CABEM NO SEU BOLSO</h2>
            <div class="text">Temos o plano certo para você</div>
        </div>

        <div class="row clearfix">

            <!--Pricing Column-->
            <div class="pricing-column col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="upper-box">
                        <h3>BÁSICO</h3>
                        <div class="price"><sup>R$</sup>40,00 <span>Por<br>Veículo</span></div>
                        <div class="left-white-curve"></div>
                        <div class="right-white-curve"></div>
                    </div>

                    <div class="lower-box">
                        <ul class="check-list">
                            <li class="check">Rastreamento em tempo real</li>
                            <li class="check">Chip uma operadora</li>
                            <li class="check">Acesso pelo site e aplicativo <br> para Android ou iOS</li>
                            <li class="check">Alertas: ignição ligada e desligada, excesso de velocidade, vibração, corte de alimentação, entrada ou saída de cerca virtual</li>
                            <li class="check">Aparelho: R$299,00</li>
                            <li class="check">Adesão: R$0,00</li>
                            <li class="check">Instalação: R$70,00</li>
                            <li class="cross">Equipe de Recuperação</li>
                            <li class="cross">Atendimento 24h através do 0800</li>
                            <li class="cross">Reboque</li>
                            <li class="cross">Pane Seca</li>
                            <li class="cross">Chaveiro</li>
                            <li class="cross">Troca de Pneu</li>
                            <li class="cross">Central de Assistência Veicular 24H</li>
                        </ul>

                        <div class="link-box">
                            <a href="https://wa.me/558533001816" target="_blank" class="buy-now theme-btn btn-style-one">CONTRATAR</a>
                        </div>

                    </div>

                </div>
            </div>

            <!--Pricing Column-->
            <div class="pricing-column col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="upper-box">
                        <h3>INTERMEDIÁRIO</h3>
                        <div class="price"><sup>R$</sup>50,00 <span>Por<br>Veículo</span></div>
                        <div class="left-white-curve"></div>
                        <div class="right-white-curve"></div>
                    </div>

                    <div class="lower-box">
                        <ul class="check-list">
                            <li class="check">Rastreamento em tempo real</li>
                            <li class="check">Chip multi operadora</li>
                            <li class="check">Acesso pelo site e aplicativo <br> para Android ou iOS</li>
                            <li class="check">Alertas: ignição ligada e desligada, excesso de velocidade, vibração, corte de alimentação, entrada ou saída de cerca virtual</li>
                            <li class="check">Aparelho: COMODATO</li>
                            <li class="check">Adesão: R$0,00</li>
                            <li class="check">Instalação: R$70,00</li>
                            <li class="check">Equipe de Recuperação</li>
                            <li class="check">Atendimento 24h através do 0800</li>
                            <li class="cross">Reboque</li>
                            <li class="cross">Pane Seca</li>
                            <li class="cross">Chaveiro</li>
                            <li class="cross">Troca de Pneu</li>
                            <li class="cross">Central de Assistência Veicular 24H</li>
                        </ul>

                        <div class="link-box">
                            <a href="https://wa.me/558533001816" target="_blank" class="buy-now theme-btn btn-style-one">CONTRATAR</a>
                        </div>

                    </div>

                </div>
            </div>

            <!--Pricing Column-->
            <div class="pricing-column col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="upper-box">
                        <h3>PREMIUM</h3>
                        <div class="price"><sup>R$</sup>60,00 <span>Por<br>Veículo</span></div>
                        <div class="left-white-curve"></div>
                        <div class="right-white-curve"></div>
                    </div>

                    <div class="lower-box">
                        <ul class="check-list">
                            <li class="check">Rastreamento em tempo real</li>
                            <li class="check">Chip multi operadora</li>
                            <li class="check">Acesso pelo site e aplicativo <br> para Android ou iOS</li>
                            <li class="check">Alertas: ignição ligada e desligada, excesso de velocidade, vibração, corte de alimentação, entrada ou saída de cerca virtual</li>
                            <li class="check">Aparelho: COMODATO</li>
                            <li class="check">Adesão: R$0,00</li>
                            <li class="check">Instalação: R$70,00</li>
                            <li class="check">Equipe de Recuperação</li>
                            <li class="check">Atendimento 24h através do 0800</li>
                            <li class="check">Reboque</li>
                            <li class="check">Pane Seca</li>
                            <li class="check">Chaveiro</li>
                            <li class="check">Troca de Pneu</li>
                            <li class="check">Central de Assistência Veicular 24H</li>
                        </ul>

                        <div class="link-box">
                            <a href="https://wa.me/558533001816" target="_blank" class="buy-now theme-btn btn-style-one">CONTRATAR</a>
                        </div>

                    </div>

                </div>
            </div>

        </div>

    </div>
</section>
<!--End Price Section-->

   <!--Security Section-->
<section class="security-section" style="background-image:url(/site/images/background/3.png);">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="form-column col-md-5 col-sm-6 col-xs-12">
                <!--Security Form-->
                <div class="security-form default-form">
                    <h2>SOLICITE SEU ORÇAMENTO PERSONALIZADO</h2>

                    <form method="post" action="/formularioContato">
                        {!! csrf_field() !!}
                        <div class="clearfix">
                            <div class="form-group">
                                <div class="group-inner">
                                    <label class="icon-label" for="field-one"><span class="fa flaticon-social"></span></label>
                                    <input type="text" name="nome" placeholder="Nome *" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="group-inner">
                                    <label class="icon-label" for="field-two"><span class="fa flaticon-envelope-2"></span></label>
                                    <input type="email" name="email" placeholder="E-mail">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="group-inner">
                                    <label class="icon-label" for="field-three"><span class="fa fa-mobile"></span></label>
                                    <input class="phone" type="text" name="telefone" placeholder="Telefone *" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="group-inner">
                                    <select name="quantidade">
                                        <option>Quantos veículos?</option>
                                        <option value="1">3 à 10</option>
                                        <option value="2">11 à 50</option>
                                        <option value="3">Acima 50</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group icon-group">
                                <div class="group-inner">
                                    <textarea id="field-five" name="mensagem" placeholder="Mensagem"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="theme-btn btn-style-one">SOLICITAR <span class="icon flaticon-right-arrow"></span></button>
                            </div>

                        </div>
                    </form>

                </div>
            </div>
            <div class="content-column col-md-7 col-sm-6 col-xs-12">

            </div>
        </div>
    </div>
</section>
<!--End Security Section-->
@endsection

@section('js')
@endsection

@section('script')
@endsection



