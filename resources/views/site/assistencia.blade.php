@extends('templates.site')

@section('css')
@endsection

@section('corpo')

<!--Page Title-->
<section class="page-title" style="background-image:url(/site/images/background/4.jpg);">
    <div class="auto-container">
        <div class="inner-box">
            <h1>Fique zen e deixe tudo com a gente</h1>
        </div>
    </div>
</section>
<!--End Page Title-->

   <!--Welcome Section-->
   <section class="welcome-section">
    <div class="auto-container">
        <div class="row clearfix">

            <!--Content Column / Fact Counter Two-->
            <div class="content-column col-md-8 col-sm-12 col-xs-12">
                <div class="inner-box">
                    <h3>ASSISTÊNCIA 24H</h3>
                    <div class="text">Nós temos o melhor atendimento 0800 para sua empresa. Assistência 24/7 para seus veículos em parceria com a <a href="https://www.numclickassistencias.com.br/">NumClick Assistências.</a></div>

                    <!--Fact Counter Two-->
                    <div class="fact-counter">
                        <div class="clearfix">

                            <!--Column-->
                            <div class="inner-column light-bg counter-column wow fadeIn">
                                <div class="inner">
                                    <div class="icon-box"><span class="icon flaticon-headphones"></span></div>
                                    <div class="count-outer">
                                        <span class="count-text" data-speed="4000" data-stop="3561">0</span>
                                    </div>
                                    <h4 class="counter-title">Atendimentos</h4>
                                </div>
                            </div>

                            <!--Column-->
                            <div class="inner-column counter-column wow fadeIn">
                                <div class="inner">
                                    <div class="icon-box"><span class="icon flaticon-earth"></span></div>
                                    <div class="count-outer">
                                        <span class="count-text" data-speed="3000" data-stop="4536">0</span>
                                    </div>
                                    <h4 class="counter-title">Km de reboque ultilizado<small></small></h4>
                                </div>
                            </div>

                            <!--Column-->
                            <div class="inner-column counter-column wow fadeIn">
                                <div class="inner">
                                    <div class="icon-box"><span class="icon flaticon-map-marker-1"></span></div>
                                    <div class="count-outer">
                                        <span class="count-text" data-speed="2000" data-stop="27">0</span>
                                    </div>
                                    <h4 class="counter-title">Estados atendidos</h4>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
            <!--Video Column-->
            <div class="column col-md-4 col-sm-12 col-xs-12">
                <div class="video-box">
                    <figure class="image">
                        <img src="/site/images/resource/video-image.png" alt="">
                    </figure>
                </div>
            </div>

        </div>
    </div>
</section>
<!--Featured Section-->
<section class="featured-section">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Featured Post-->
            <div class="featured-post col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="number-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">01</div>
                    <h3><a href="services-single.html">Reboque 100km</a></h3>
                    <div class="text">Com até 3 remoções por ano, o Serviço de Guincho cobre até 100km em todo Brasil.</div>
                </div>
            </div>
            <!--End Featured Post-->

            <!--Featured Post-->
            <div class="featured-post col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="number-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">02</div>
                    <h3><a href="services-single.html">Aux. Mecânico</a></h3>
                    <div class="text">Com o serviço de auxílio mecânico o cliente não fica na mão! Em caso de pane mecânica ou elétrica será enviado um profissional para ajudar em um reparo emergencial no local.</div>
                </div>
            </div>
            <!--End Featured Post-->

            <!--Featured Post-->
            <div class="featured-post col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="number-box wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">03</div>
                    <h3><a href="services-single.html">Chaveiro</a></h3>
                    <div class="text">Tenha ajuda em caso de perda da chave, esquecimento no interior do veículo e problemas com a fechadura.</div>
                </div>
            </div>
            <!--End Featured Post-->
        </div>
        <div class="row clearfix">
            <!--Featured Post-->
            <div class="featured-post col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="number-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">04</div>
                    <h3><a href="services-single.html">Troca de Pneus</a></h3>
                    <div class="text">Em caso de dano no pneu do veículo, será enviado um profissional para troca pelo estepe ou reboque até uma borracharia que possa consertá-lo.</div>
                </div>
            </div>
            <!--End Featured Post-->

            <!--Featured Post-->
            <div class="featured-post col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="number-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">05</div>
                    <h3><a href="services-single.html">Pane Seca</a></h3>
                    <div class="text">A Fortech GPS te ajuda até mesmo na falta de combustível! Conte com um reboque para levar o veículo até um posto de serviço mais próximo.</div>
                </div>
            </div>
            <!--End Featured Post-->

            <!--Featured Post-->
            <div class="featured-post col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="number-box wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">06</div>
                    <h3><a href="services-single.html">Território Brasileiro</a></h3>
                    <div class="text">Atendimento em todo o território Brasileiro com 0800 24 horas.</div>
                </div>
            </div>
            <!--End Featured Post-->

        </div>
    </div>
</section>
<!--End Featured Section-->

@endsection

@section('js')
@endsection

@section('script')
@endsection



