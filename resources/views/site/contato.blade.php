@extends('templates.site')

@section('css')
@endsection

@section('corpo')

<!--Page Title-->
<section class="page-title" style="background-image:url(/site/images/background/4.jpg);">
    <div class="auto-container">
        <div class="inner-box">
            <h1>Fique zen e deixe tudo com a gente</h1>
        </div>
    </div>
</section>
<!--End Page Title-->

<!--Contact Form-->
<div class="contact-form-section">
    <div class="auto-container">

           <!-- contact Form -->
        <div class="contact-form">

            <div class="group-title"><h2>Envie-nos uma mensagem</h2>
                <P>A equipe da <strong>Fortech GPS</strong> ficará feliz com o seu contato e terá o prazer de respondê-lo assim que possível.</P>
            </div>

            <!--Comment Form-->
            <form method="post" action="/formularioContato">
                {!! csrf_field() !!}
                <div class="row clearfix">

                    <div class="form-group col-md-4 col-sm-12 col-xs-12">
                        <div class="group-inner">
                            <label class="icon-label" for="field-one"><span class="fa flaticon-social"></span></label>
                            <input type="text" name="nome" placeholder="Nome" required>
                        </div>
                    </div>

                    <div class="form-group col-md-4 col-sm-12 col-xs-12">
                        <div class="group-inner">
                            <label class="icon-label" for="field-two"><span class="fa flaticon-phone-call"></span></label>
                            <input type="text" class="phone" name="telefone" placeholder="Telefone" required>
                        </div>
                    </div>

                    <div class="form-group col-md-4 col-sm-12 col-xs-12">
                        <div class="group-inner">
                            <label class="icon-label" for="field-two"><span class="fa flaticon-envelope-2"></span></label>
                            <input type="email" name="email" placeholder="E-mail">
                        </div>
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <textarea class="darma" name="mensagem" placeholder="Mensagem"></textarea>
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <button class="theme-btn btn-style-one" type="submit" name="submit-form">ENVIAR</button>
                    </div>

                </div>
            </form>
        </div>
        <!--End Comment Form -->

    </div>
</div>
<!--End Contact Form-->



@endsection

@section('js')
@endsection

@section('script')
@endsection



