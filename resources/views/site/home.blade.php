@extends('templates.site')

@section('css')
@endsection

@section('corpo')

 <!--Main Slider-->
 <section class="main-slider" data-start-height="820" data-slide-overlay="yes">

    <div class="tp-banner-container">
        <div class="tp-banner">
            <ul>
                <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="/site/images/main-slider/1.jpg"  data-saveperformance="off"  data-title="Awesome Title Here">
                <img src="/site/images/main-slider/Frota-Caminhoes-Rastreados.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">

                    <div class="tp-caption sft sfb tp-resizeme"
                    data-x="left" data-hoffset="15"
                    data-y="center" data-voffset="-60"
                    data-speed="1500"
                    data-start="0"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn">
                        <div class="text">Os veículos de sua empresa representam uma parcela do funcionamento do seu negócio e merecem proteção e atenção,</div>
                    </div>

                    <div class="tp-caption sft sfb tp-resizeme"
                    data-x="left" data-hoffset="15"
                    data-y="center" data-voffset="20"
                    data-speed="1500"
                    data-start="500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn">
                        <h2>já que seu prejuízo pode ser imensurável.</h2>
                    </div>

                    <div class="tp-caption sft sfb tp-resizeme"
                    data-x="left" data-hoffset="15"
                    data-y="center" data-voffset="110"
                    data-speed="1500"
                    data-start="1000"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn">
                        <div class="btn-box"><a href="https://wa.me/558533001816" target="_blank" class="theme-btn btn-style-one">Solicite seu Orçamento.</a></div>
                    </div>

                </li>

                <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="/site/images/main-slider/2.jpg"  data-saveperformance="off"  data-title="Awesome Title Here">
                <img src="/site/images/main-slider/Mecanico-Assistencia-24h.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">

                    <div class="tp-caption sft sfb tp-resizeme"
                    data-x="center" data-hoffset="0"
                    data-y="center" data-voffset="-60"
                    data-speed="1500"
                    data-start="0"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn">
                        <div class="text">Assistência Automotiva 24h Fortech GPS: <br>
                            Mais segurança para o seu veículo e comodidade onde você estiver!</div>
                    </div>


                    <div class="tp-caption sft sfb tp-resizeme"
                    data-x="center" data-hoffset="-135"
                    data-y="center" data-voffset="30"
                    data-speed="1500"
                    data-start="1000"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn">
                        <div class="btn-box"><a href="https://wa.me/558533001816" target="_blank" class="theme-btn btn-style-one">Solicite seu Orçamento.</a></div>
                    </div>


                </li>

            </ul>

            <div class="tp-bannertimer"></div>
        </div>
    </div>
</section>
<!--End Main Slider-->

<!--Inquiry Form-->
<section class="inquiry-section wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
    <div class="auto-container">
        <h3>Solicite Seu Orçamento</h3>

        <!--Inquiry Form-->
        <div class="inquiry-form">
            <form method="post" action="/formularioContato">
                {!! csrf_field() !!}
                <div class="row clearfix">
                    <!--Form Group-->
                    <div class="form-group col-md-4 col-sm-6 col-xs-12">
                        <div class="group-inner">
                            <label class="icon-label" for="field-one"><span class="flaticon-social"></span></label>
                            <input id="field-one" type="text" name="nome" value="" placeholder="Nome" required>
                        </div>
                    </div>
                    <!--Form Group-->
                    <div class="form-group col-md-4 col-sm-6 col-xs-12">
                        <div class="group-inner">
                            <label class="icon-label" for="field-two"><span class="flaticon-business-1"></span></label>
                            <input id="field-two" type="email" name="email" value="" placeholder="E-Mail" required>
                        </div>
                    </div>
                    <!--Form Group-->
                    <div class="form-group col-md-4 col-sm-6 col-xs-12">
                        <div class="group-inner">
                            <label class="icon-label" for="field-three"><span class="flaticon-smartphone"></span></label>
                            <input class="phone" type="text" name="telefone" placeholder="Celular">
                        </div>
                    </div>
                    <!--Form Group-->
                    <div class="form-group col-md-4 col-sm-6 col-xs-12">
                        <div class="group-inner">
                            <select name="veiculo">
                                <option value="1">Moto</option>
                                <option value="2">Carro</option>
                                <option value="3">Caminhão</option>
                                <option value="4">Outros</option>
                            </select>
                        </div>
                    </div>
                    <!--Form Group-->
                    <div class="form-group col-md-4 col-sm-12 col-xs-12">
                        <div class="group-inner">
                            <label class="icon-label" for="field-four"><span class="flaticon-speech-bubble-1"></span></label>
                            <textarea id="field-four" name="mensagem" placeholder="Sua Mensagem"></textarea>
                        </div>
                    </div>
                    <!--Form Group-->
                    <div class="form-group col-md-4 col-sm-12 col-xs-12">
                        <button type="submit" class="theme-btn btn-style-three">RECEBER ORÇAMENTO <span class="fa fa-arrow-right"></span></button>
                    </div>

                </div>
            </form>
        </div>
        <!--End Inquiry Form-->

    </div>
</section>
<!--End Inquiry Form-->

<!--Services Section-->
<section class="services-section">
    <div class="auto-container">
        <!--Sec Title-->
        <div class="sec-title">
            <h1>RASTREAMENTO VEICULAR</h1>
            <div class="text"></div>
        </div>

        <div class="outer-box" style="background-image: url(/site/images/background/mapa.png);">
            <div class="row clearfix">

                <!--Left Column-->
                <div class="left-column col-md-4 col-sm-6 col-xs-12 pull-left">
                    <!--Service Block One-->
                    <div class="service-block-one">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon flaticon-headphones"></span>
                            </div>
                            <h3><a>CENTRAL 0800 24h/7</a></h3>
                            <div class="text">Nossa Central 0800 24h acompanha seu veículo 24 horas por dia, durante os 365 dias do ano.</div>
                        </div>
                    </div>

                    <!--Service Block One-->
                    <div class="service-block-one">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon flaticon-policeman-1"></span>
                            </div>
                            <h3><a>EQUIPE DE RECUPERAÇÃO</a></h3>
                            <div class="text">O serviço de pronta resposta proporciona rápida ação para prevenção contra sinistros, recuperação de veículos, cargas ou equipamentos rastreados. </div>
                        </div>
                    </div>

                    <!--Service Block One-->
                    <div class="service-block-one">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon flaticon-danger"></span>
                            </div>
                            <h3><a>VARIOS ALERTAS</a></h3>
                            <div class="text">Vibração, limite de velocidade, desligamento de bateria, saída de cerca, etc.</div>
                        </div>
                    </div>

                </div>

                <!--Right Column-->
                <div class="right-column col-md-4 col-sm-6 col-xs-12 pull-right">
                    <!--Service Block Two-->
                    <div class="service-block-two">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon flaticon-map"></span>
                            </div>
                            <h3><a>CERCA VIRTUAL</a></h3>
                            <div class="text">O recurso funciona como uma cerca eletrônica que registra quando um veículo entra ou sai do perímetro definido.</div>
                        </div>
                    </div>

                    <!--Service Block Two-->
                    <div class="service-block-two">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon flaticon-geography"></span>
                            </div>
                            <h3><a>CHIP MULTI OPERADORA</a></h3>
                            <div class="text">O Rastreador estará pesquisando pela operadora com a melhor qualidade de sinal.</div>
                        </div>
                    </div>

                    <!--Service Block Two-->
                    <div class="service-block-two">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon flaticon-close"></span>
                            </div>
                            <h3><a>BLOQUEIO PELO APLICATIVO</a></h3>
                            <div class="text">Em caso de furto, acionando o bloqueio, o carro, moto ou caminhão é “travado” imediatamente.</div>
                        </div>
                    </div>

                </div>

                <!--Image Column-->
                <div class="image-column col-md-4 col-sm-12 col-xs-12">
                    <figure class="image-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <img src="/site/images/resource/aplicativo-rastreamento-veicular.png" alt="Aplicativo rastreamento fortech GPS" />
                    </figure>
                </div>

            </div>
        </div>

    </div>
</section>
<!--End Services Section-->
<!--Call to Action-->
<section class="call-to-action" style="background-image:url(/site/images/background/2.jpg);">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Content Column-->
            <div class="content-column col-md-9 col-sm-9 col-xs-12">
                <div class="inner-box">
                    <div class="icon-box">
                        <div class="icon flaticon-note"></div>
                    </div>
                    <div class="content">
                        <h3>SOLICITE AGORA SEU ORÇAMENTO: <a target="_blank" style="color:white;" href="https://wa.me/558533001816">(85)3300-1816</a></h3>
                        <div class="text">Localize, acompanhe e controle o que acontece com seu veículo mesmo quando não estiver por perto.</div>
                    </div>
                </div>
            </div>

            <div class="btn-column text-right col-md-3 col-sm-3 col-xs-12">
                <a href="#" class="theme-btn btn-style-one">Solicite seu Orçamento.</a>
            </div>

        </div>
    </div>
</section>
<!--Call to Action-->
<!--Why Us Section-->
<section class="why-us-section">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Content Column-->
            <div class="content-column col-md-6 col-sm-12 col-xs-12">


                <!--Service Block Two-->
                <div class="service-block-two style-two">
                    <div class="inner-box">

                        <h3><a href="services-single.html">Reboque</a></h3>
                        <div class="text">Reboque até a oficina ou concessionária de escolha do Usuário ou indicada pela Assistência 24 horas.</div>
                    </div>
                </div>

                <!--Service Block Two-->
                <div class="service-block-two style-two">
                    <div class="inner-box">

                        <h3><a href="services-single.html">Socorro Mecânico ou Elétrico</a></h3>
                        <div class="text">SOS Mecânico para tentar executar o reparo emergencial no local.</div>
                    </div>
                </div>

                <!--Service Block Two-->
                <div class="service-block-two style-two">
                    <div class="inner-box">

                        <h3><a href="services-single.html">Troca de pneus</a></h3>
                        <div class="text">Envio de profissional para substituição do pneu pelo estepe ou reboque do veículo até o borracheiro mais próximo</div>
                    </div>
                </div>

                <!--Service Block Two-->
                <div class="service-block-two style-two">
                    <div class="inner-box">

                        <h3><a href="services-single.html">Chaveiro</a></h3>
                        <div class="text">Chaveiro para abertura do veículo ou retirada da chave quebrada da porta, ignição ou tanque de combustível.</div>
                    </div>
                </div>

            </div>
            <!--Counter Column-->
            <div class="counter-column col-md-6 col-sm-12 col-xs-12" >

                <img src="/site/images/resource/assistencia-24h.jpg" alt="Mecanico na Assistência 24h">

            </div>
        </div>
    </div>
</section>
<!--Why Us Section-->
@endsection

@section('js')
@endsection

@section('script')

@endsection



