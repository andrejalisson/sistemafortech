<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ChipsController extends Controller{
    public function lista(){
        $title = "Chips";
        return view('chips.lista')->with(compact('title'));
    }

    public function todosChips(Request $request){
        $columns = array(
            0 =>'id',
            1 =>'fornecedor_id',
            2 =>'operadoras_id',
            3 =>'iccid',
            4 =>'mb',
            5 =>'status',
        );

        $totalData = DB::table('operadoras')->count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if(empty($request->input('search.value'))){
            $operadoras = DB::table('operadoras')->offset($start)->limit($limit)->orderBy($order,$dir)->get();
        }
        else{
            $search = $request->input('search.value');
            $operadoras =  DB::table('operadoras')
                ->where('nome','LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
            $totalFiltered = DB::table('operadoras')->where('nome','LIKE',"%{$search}%")->count();
        }
        $data = array();

        if(!empty($operadoras)){
            foreach ($operadoras as $operadora){
                $nestedData['id'] = "# ".$operadora->id;
                $nestedData['nome'] = strtoupper($operadora->nome);
                if($operadora->multi == 1){
                    $nestedData['multi'] = "<span class=\"badge badge-light-success\">SIM</span>";
                }else{
                    $nestedData['multi'] = "<span class=\"badge badge-light-danger\">NÃO</span>";
                }

                if($operadora->status == 1){
                    $nestedData['status'] = "<span class=\"badge badge-light-success\">ATIVO</span>";
                    $nestedData['opcoes'] = "<div class='text-end'>
                    <button onclick=\"desativar($operadora->id)\" class=\"btn btn-danger\"><i class=\"fas fa-times fs-4 me-2\"></i> Desativar</button>
                </div>";
                }else{
                    $nestedData['status'] = "<span class=\"badge badge-light-danger\">DESATIVADO</span>";
                    $nestedData['opcoes'] = "<div class='text-end'>
                    <button onclick=\"ativar($operadora->id)\" class=\"btn btn-success\"><i class=\"fas fa-times fs-4 me-2\"></i> Ativar</button>
                </div>";
                }

                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        echo json_encode($json_data);
    }


}
