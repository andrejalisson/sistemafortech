<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OperadorasController extends Controller{
    public function lista(){
        $title = "Operadoras";
        return view('operadoras.lista')->with(compact('title'));
    }

    public function todasOperadoras(Request $request){
        $columns = array(
            0 =>'id',
            1 =>'nome',
            2 =>'multi',
            3 =>'status',
        );

        $totalData = DB::table('operadoras')->count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if(empty($request->input('search.value'))){
            $operadoras = DB::table('operadoras')->offset($start)->limit($limit)->orderBy($order,$dir)->get();
        }
        else{
            $search = $request->input('search.value');
            $operadoras =  DB::table('operadoras')
                ->where('nome','LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
            $totalFiltered = DB::table('operadoras')->where('nome','LIKE',"%{$search}%")->count();
        }
        $data = array();

        if(!empty($operadoras)){
            foreach ($operadoras as $operadora){
                $nestedData['id'] = "# ".$operadora->id;
                $nestedData['nome'] = strtoupper($operadora->nome);
                if($operadora->multi == 1){
                    $nestedData['multi'] = "<span class=\"badge badge-light-success\">SIM</span>";
                }else{
                    $nestedData['multi'] = "<span class=\"badge badge-light-danger\">NÃO</span>";
                }

                if($operadora->status == 1){
                    $nestedData['status'] = "<span class=\"badge badge-light-success\">ATIVO</span>";
                    $nestedData['opcoes'] = "<div class='text-end'>
                    <button onclick=\"desativar($operadora->id)\" class=\"btn btn-danger\"><i class=\"fas fa-times fs-4 me-2\"></i> Desativar</button>
                </div>";
                }else{
                    $nestedData['status'] = "<span class=\"badge badge-light-danger\">DESATIVADO</span>";
                    $nestedData['opcoes'] = "<div class='text-end'>
                    <button onclick=\"ativar($operadora->id)\" class=\"btn btn-success\"><i class=\"fas fa-times fs-4 me-2\"></i> Ativar</button>
                </div>";
                }

                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        echo json_encode($json_data);
    }

    public function addOperadora(Request $request){
        DB::table('operadoras')->insert([
            'nome' => $request->operadora,
            'multi' => $request->multi,
            'status' => 1,
            ]
        );
        $resposta['mensagem'] = "true";
        echo json_encode($resposta);
    }

    public function statusOperadora(Request $request){
        $operadora = DB::table('operadoras')->where('id',$request->id)->first();
        if($operadora->status == 1){
            DB::table('operadoras')->where('id',$request->id)->update(['status' => 0]);
        }else{
            DB::table('operadoras')->where('id',$request->id)->update(['status' => 1]);
        }
        $resposta['mensagem'] = "true";
        echo json_encode($resposta);
    }
}
