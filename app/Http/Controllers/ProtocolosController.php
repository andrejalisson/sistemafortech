<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ProtocolosController extends Controller{
    public function lista(){
        $title = "Protocolos";
        return view('protocolos.lista')->with(compact('title'));
    }

    public function todosProtocolos(Request $request){
        $columns = array(
            0 =>'id',
            1 =>'nome',
            2 =>'porta',
        );

        $totalData = DB::table('protocolos')->count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if(empty($request->input('search.value'))){
            $protocolos = DB::table('protocolos')->offset($start)->limit($limit)->orderBy($order,$dir)->get();
        }
        else{
            $search = $request->input('search.value');
            $protocolos =  DB::table('protocolos')->where('nome','LIKE',"%{$search}%")->orWhere('porta','LIKE',"%{$search}%")->offset($start)->limit($limit)->orderBy($order,$dir)->get();
            $totalFiltered = DB::table('protocolos')->where('nome','LIKE',"%{$search}%")->orWhere('porta','LIKE',"%{$search}%")->count();
        }
        $data = array();
        if(!empty($protocolos)){
            foreach ($protocolos as $protocolo){
                $nestedData['id'] = "# ".$protocolo->id;
                $nestedData['nome'] = strtoupper($protocolo->nome);
                $nestedData['porta'] = $protocolo->porta;
                $nestedData['status'] = "<span class=\"badge badge-light-success\">ATIVO</span>";
                $nestedData['opcoes'] = "<div class='text-end'>
                    <a href=\"#\" class=\"btn btn-danger\"><i class=\"fas fa-times fs-4 me-2\"></i> Desativar</a>
                </div>";
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        echo json_encode($json_data);
    }
}
