<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MonitoramentoController extends Controller{

    public function cliente(){
        $title = "Monitoramento";
        $localizacao = DB::connection('traccar')->table('tc_positions')->orderBy('id', 'desc')->first();
        return view('monitoramento.cliente')->with(compact('title', 'localizacao'));
    }
}
