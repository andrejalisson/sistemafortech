<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ModelosController extends Controller{
    public function lista(){
        $title = "Modelos";
        return view('modelos.lista')->with(compact('title'));
    }

    public function todosModelos(Request $request){
        $columns = array(
            0 =>'id',
            1 =>'modelo',
            2 =>'protocolos_id',
        );

        $totalData = DB::table('modelos')->count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if(empty($request->input('search.value'))){
            $modelos = DB::table('modelos')->select("protocolos.nome", "protocolos.porta","modelos.*")->leftJoin('protocolos', 'protocolos_id', '=', 'protocolos.id')->offset($start)->limit($limit)->orderBy($order,$dir)->get();
        }
        else{
            $search = $request->input('search.value');
            $modelos =  DB::table('modelos')
                        ->select("protocolos.nome", "protocolos.porta","modelos.*")
                        ->leftJoin('protocolos', 'protocolos_id', '=', 'protocolos.id')
                        ->where('modelo','LIKE',"%{$search}%")
                        ->orWhere('protocolos.nome','LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
            $totalFiltered = DB::table('modelos')->leftJoin('protocolos', 'protocolos_id', '=', 'protocolos.id')->where('modelo','LIKE',"%{$search}%")->orWhere('nome','LIKE',"%{$search}%")->count();
        }
        $data = array();

        if(!empty($modelos)){
            foreach ($modelos as $modelo){
                $nestedData['id'] = "# ".$modelo->id;
                $nestedData['modelo'] = strtoupper($modelo->modelo);
                $nestedData['protocolo'] = strtoupper($modelo->nome);
                $nestedData['porta'] = strtoupper($modelo->porta);
                $nestedData['status'] = "<span class=\"badge badge-light-success\">ATIVO</span>";
                $nestedData['opcoes'] = "<div class='text-end'>
                    <a href=\"#\" class=\"btn btn-danger\"><i class=\"fas fa-times fs-4 me-2\"></i> Desativar</a>
                </div>";
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        echo json_encode($json_data);
    }
}
