<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuarios;

class AcessoController extends Controller{

    public function login(){
        $title = "Login";
        return view('acesso.login')->with(compact('title'));
    }

    public function verificaAjax(Request $request){
        $usuario = Usuarios::where('email', $request->email)->orWhere('usuario', $request->email)->first();
        if($usuario != null){
            if(password_verify($request->senha, $usuario->senha )){
                $resposta['mensagem'] = "Você fez login com sucesso!";
            }else{
                $resposta['erro'] = "true";
                $resposta['mensagem'] = "Senha está incorreta";
            }
        }else{
            $resposta['erro'] = "true";
            $resposta['mensagem'] = "Usuário não encontrado";

        }
        echo json_encode($resposta);
    }

    public function verifica(Request $request){
        $usuario = Usuarios::where('email', $request->email)->orWhere('usuario', $request->email)->first();
        $request->session()->flash('sucesso', 'Bom trabalho!');
        $request->session()->put('id', $usuario->id);
        $request->session()->put('nome', $usuario->nome);
        $request->session()->put('email', $usuario->email);
        $request->session()->put('logado', true);
        $request->session()->put('tipo', $usuario->tipo);
        switch ($usuario->tipo) {
            case 1:
                return redirect('/Monitoramento');
                break;
            case 2:
                return redirect('/Monitoramento');
                break;
            case 3:
                return redirect('/Monitoramento');
                break;
        }

    }

    public function reset(){
        $title = "Reset Senha";
        return view('acesso.reset')->with(compact('title'));
    }
}
