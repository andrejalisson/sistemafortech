<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Queue\Jobs\Job;
use Illuminate\Support\Facades\Mail;

class SiteController extends Controller{
    public function home(){
        $title = "Home";
        return view('site.home')->with(compact('title'));
    }

    public function planos(){
        $title = "Planos";
        return view('site.planos')->with(compact('title'));
    }

    public function assistencia(){
        $title = "Assistência 24H";
        return view('site.assistencia')->with(compact('title'));
    }

    public function contato(){
        $title = "Contato";
        return view('site.contato')->with(compact('title'));
    }

    public function formularioContato(Request $request){
//         DB::table('contatos')
//         ->insert([
//             'nome' => $request->nome,
//             'email' => $request->email,
//             'telefone' => $request->telefone,
//             'veiculo' => $request->veiculo,
//             'quantidade' => $request->quantidade,
//             'mensagem' => $request->mensagem,
//             'criado' => date('Y-m-d H:i:s')
//         ]);
        $contato = new \stdClass();
        $contato->nome = $request->nome;
        $contato->email = $request->email;
        $contato->celular = $request->telefone;
        $contato->veiculo = $request->veiculo;
        $contato->quantidade = $request->quantidade;
        $contato->Mensagem = $request->mensagem;
        $contato->criado = date('Y-m-d H:i:s');
        \App\Jobs\newContato::dispatch($contato);
        $request->session()->flash('sucesso', 'Um de nossos consultores logo entrará em contato!');
        return redirect()->back();
    }
}
