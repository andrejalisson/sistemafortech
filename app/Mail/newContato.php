<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class newContato extends Mailable
{
    use Queueable, SerializesModels;
    private $contato;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(\stdClass $contato){
        $this->contato = $contato;
        $this->subject("Novo contato do site FORTECH GPS");
        $this->to('contato@fortechgps.com.br','Site Fortech');
        $this->cc('andrejalisson@gmail.com','André Jálisson');
//        $this->cc('bezerra260@gmail.com','Marcelo Bezerra');
        return $this->markdown('mails.newContato', [
            'contato' => $this->contato
        ]);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('view.name');
    }
}
