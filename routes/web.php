<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SiteController;
use App\Http\Controllers\AcessoController;
use App\Http\Controllers\MonitoramentoController;
use App\Http\Controllers\ProtocolosController;
use App\Http\Controllers\ModelosController;
use App\Http\Controllers\OperadorasController;
use App\Http\Controllers\ChipsController;


//SITE
Route::get('/', [SiteController::class, 'home']);
Route::get('/Planos', [SiteController::class, 'planos']);
Route::get('/Assistencia', [SiteController::class, 'assistencia']);
Route::get('/Contato', [SiteController::class, 'contato']);
Route::post('/formularioHome', [SiteController::class, 'formularioHome']);
Route::post('/formularioPlanos', [SiteController::class, 'formularioPlanos']);
Route::post('/formularioContato', [SiteController::class, 'formularioContato']);

//ACESSO
Route::get('/Login',[AcessoController::class, 'login']);
Route::post('/Login',[AcessoController::class, 'verificaAjax'])->name('login');
Route::post('/Verifica',[AcessoController::class, 'verifica']);
Route::get('/ResetSenha',[AcessoController::class, 'reset']);

//MONITORAMENTO
Route::get('/Monitoramento', [MonitoramentoController::class, 'cliente']);

//PROTOCOLOS
Route::get('/Protocolos', [ProtocolosController::class, 'lista']);
Route::post('/todosProtocolos', [ProtocolosController::class, 'todosProtocolos'])->name('todosProtocolos');

//MODELOS
Route::get('/Modelos', [ModelosController::class, 'lista']);
Route::post('/todosModelos', [ModelosController::class, 'todosModelos'])->name('todosModelos');

//OPERADORAS
Route::get('/Operadoras', [OperadorasController::class, 'lista']);
Route::post('/todasOperadoras', [OperadorasController::class, 'todasOperadoras'])->name('todasOperadoras');
Route::post('/addOperadora', [OperadorasController::class, 'addOperadora']);
Route::post('/statusOperadora', [OperadorasController::class, 'statusOperadora']);

//CHIPS
Route::get('/Chips', [ChipsController::class, 'lista']);
Route::post('/todosChips', [ChipsController::class, 'todosChips'])->name('todosChips');
Route::post('/addChip', [ChipsController::class, 'addChip']);
Route::post('/statusChip', [ChipsController::class, 'statusChip']);

